import React, {useRef, useState} from 'react'
import ErrorModal from '../components/ErrorModal';



export default function Connexion() {

    const emailInputRef = useRef();
    const passwordInputRef = useRef();

    const [error, setError] = useState();

    // controler si error est a true ou false

    console.log(typeof error);

    if (error) {
        console.log("true")
    }else(
        console.log("false")
    )

    const submitHandler = (event) => {
        event.preventDefault()

        const enteredEmail = emailInputRef.current.value;
        const enteredPassword = passwordInputRef.current.value;

        // controle input pas vide
        if (enteredEmail.trim().length === 0 || enteredPassword.trim().length === 0 ) {
            setError({
                title: "Merci de compléter les champs manquants",
            })
            return;
        }

        //controle validité email
        const regexEmail = (value) =>{
            return /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(value);
        }
        if (!regexEmail(enteredEmail)) {
            setError({
                title: "Email Invalide, vérifier vos informations",
            })
            return;
        }

        console.log(enteredEmail, enteredPassword);
        // recuperer les infortions de connexion avec token

        const url = "https://127.0.0.1:8000/api/login_check";

        fetch(url, {
            method: "POST",
            body: JSON.stringify({
                username: enteredEmail,
                password: enteredPassword,  
            }),
            headers:{
                "Content-Type" : "application/json"
            }
        }).then((response)=>response.json())
        .then((data)=>{
            console.log(data)
        })

        //réinitialisé les champs input
        emailInputRef.current.value = "";
        passwordInputRef.current.value = "";
    }

    return (
        <>
        {error && <ErrorModal 
            title= {error.title}
            
        />}
        <section className="auth">
            <form className="mb-3" onSubmit={submitHandler}>
                <div>
                    <label className="form-label" htmlFor="email">Email </label>
                    <input className="form-control" type="email" ref={emailInputRef}  />
                    <label className="form-label" htmlFor="password">Mot de passe</label>
                    <input className="form-control" type="password" ref={passwordInputRef}  />
                    <button className="btn btn-primary"  type="submit" onClick={() => {}}>
                    Se connecter
                    </button>
                </div> 
            </form> 
        </section> 
        </>
    )

}
