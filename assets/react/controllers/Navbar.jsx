import * as React from 'react';
import { useRef } from "react";
import { FaBars, FaTimes } from "react-icons/fa";
import  LoginControl  from '../components/LoginControl';



export default function Navbar(props) {
    const navRef = useRef();

    const showNavbar = () => {
        navRef.current.classList.toggle("responsive_nav");
    };  


    
    return (
        <header>
            <a href={props.acceuil}>
                <img src={require('/public/images/logoFC.png')} width="100" height="50" ></img>
            </a>
            <nav ref={navRef}>
                <a href={props.acceuil}>Acceuil</a>
                <a href={props.tutos}>Portfolio</a>
                <a href={props.about}>Me contacter</a> 
                <a href={props.connexion}>Connexion</a>
                <button className="nav-btn nav-close-btn" onClick={showNavbar}>
                    <FaTimes />
                </button>
            </nav>
            <button className="nav-btn" onClick={showNavbar}>
                <FaBars />
            </button>
        </header>
    );
}


