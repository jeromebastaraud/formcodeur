import React from 'react'

export class LoginButton {
    constructor() {

        constructor(props); {

            this.handleLoginClick = this.handleLoginClick.bind(this);
            this.handleLogoutClick = this.handleLogoutClick.bind(this);
            this.state = { isLoggedIn: false };
        }

        handleLoginClick(); {
            this.setState({ isLoggedIn: true });
        }

        handleLogoutClick(); {
            this.setState({ isLoggedIn: false });
        }

        function LoginButton(props) {
            return <button onClick={props.onClick}>Login</button>;
        }

        function LogoutButton(props) {
            return <button onClick={props.onClick}>Logout</button>;
        }


        return (
            <div>
                <LogoutButton onClick={this.handleLogoutClick} />
                <LoginButton onClick={this.handleLoginClick} />
            </div>
        );
    }
}
