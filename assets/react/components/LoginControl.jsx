import * as React from "react";

class LoginControl extends React.Component {
    constructor(props) {
        super(props);
        // this.handleLoginClick = this.handleLoginClick.bind(this);
        // this.handleLogoutClick = this.handleLogoutClick.bind(this);
        this.state = { isLoggedIn: false };
    }

    // handleLoginClick() {
    //     this.setState({ isLoggedIn: true });
    // }

    // handleLogoutClick() {
    //     this.setState({ isLoggedIn: false });
    // }


    render() {
        const isLoggedIn = this.state.isLoggedIn;

        return (
            <div>
                <Greeting isLoggedIn={isLoggedIn} />
            </div>
        );
    }
}
{/* <LogoutButton onClick={this.handleLogoutClick} />
<LoginButton onClick={this.handleLoginClick} /> */}

function UserGreeting() {
    return <a>Deconnecter</a>;
}

function GuestGreeting() {
    return <a>Connexion</a>;
}

function Greeting(props) {
    const isLoggedIn = props.isLoggedIn;
    if (isLoggedIn) {
        return <UserGreeting />;
    }
    return <GuestGreeting />;
}

// function LoginButton(props) {
//     return <button onClick={props.onClick}>Login</button>;
// }

// function LogoutButton(props) {
//     return <button onClick={props.onClick}>Logout</button>;
// }
export default LoginControl;