<?php

namespace App\Command;

use App\Service\SendMailService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:send-mail',
    description: 'Cette commande permet d\'envoyer un mail',
)]
class SendMailCommand extends Command
{

    public function __construct( SendMailService $mailer) 
    {
        $this->mailer = $mailer;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('mail', InputArgument::REQUIRED, 'Votre adresse mail')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
            ->setHelp('Cette commande permet d\'envoyer un mail');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $mailer = $this->mailer;
        $io = new SymfonyStyle($input, $output);

        dump(get_class_methods($input));

        $mailer->sendEmail($input->getArgument('mail'));

        $io->success('Votre mail a bien été envoyé a l\'adresse suivante '. $input->getArgument('mail'));

        return Command::SUCCESS;
    }
}
