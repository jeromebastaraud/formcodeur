<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class ApiLoginController extends AbstractController
{
    // #[Route('api/login', name: 'app_api_login')]
    // public function index(#[CurrentUser] ?User $user, AuthenticationUtils $authenticationUtils): JsonResponse
    // {
    //     if ($this->getUser()) {
    //         return $this->redirectToRoute('app_home_index');
    //         $this->addFlash('success', 'Bienvenue dans votre espace personel');
    //     }
    //     //dd($this->getUser());

    //     // get the login error if there is one
    //     $error = $authenticationUtils->getLastAuthenticationError();
    //     // last username entered by the user
    //     $lastUsername = $authenticationUtils->getLastUsername();

    //     return new JsonResponse;

    //     $token = "toto";
        
    //     return $this->json([
    //         'user'  => $user->getUserIdentifier(),
    //         'token' => $token,
    //     ]);
    // }

    #[Route('api/login', name: 'app_login_check')]
    public function index(User $user): JsonResponse
    {
        $user = $this->getUser();

        if ($user) {
            return new JsonResponse(
                [
                    'email' => $user->getEmail(),
                    'roles' => $user->getRoles()
                ],
                200,
                ['Access-Control-Allow-Origin' => '*', 'Content-Type' => 'application/json']
            );
        } else {
            return new JsonResponse(
                ["message" => "Invalid user"],
                415,
                ['Access-Control-Allow-Origin' => '*', 'Content-Type' => 'application/json']
            );
        }
    }
    
}
