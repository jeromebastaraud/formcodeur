<?php

declare(strict_types=1);

namespace App\SharedAuthLibrary\Security;

use App\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class JwtUserProvider implements UserProviderInterface
{
    private JwtPayloadContainer $jwtPayloadContainer;

    public function __construct(JwtPayloadContainer $jwtPayloadContainer)
    {
        $this->jwtPayloadContainer = $jwtPayloadContainer;
    }

    public function loadUserByIdentifier($username): User
    {
        $payload = $this->jwtPayloadContainer->getPayload();

        return new User($payload['id'], $payload['email'], $payload['roles']);
        
    }

    public function refreshUser(UserInterface $user): User
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', \get_class($user))
            );
        }

        return $this->loadUserByIdentifier($user->getName());
    }

    public function supportsClass($class): bool
    {
        return User::class === $class;
    }
}
