<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


class SecurityEventSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return [
            Events::JWT_CREATED => 'onJwtCreated',
        ];
    }

    public function onJwtCreated(JWTCreatedEvent $event): void
    {
        /** @var User $user */
        $user = $event->getUser();
       

        $payload          = $event->getData();
        $payload['id']    = $user->getId();
        $payload['roles'] = $user->getRoles();
        $payload['email'] = $user->getName();
        $payload['password'] = $user->getPassword();
        $payload['exp']   = (new \DateTimeImmutable())->getTimestamp() + 86400;
        

        $event->setData($payload);
    }
}
