<?php
// src/Controller/MailerController.php
namespace App\Service;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class SendMailService extends AbstractController
{

    public function __construct(
        MailerInterface $mailer
    ) {
        $this->mailer = $mailer;
    }
    #[Route('/email')]
    public function sendEmail(string $mail): Void
    {
        $email = (new Email())
            ->from('contact@formcodeur.com')
            ->to($mail)
            //->cc('cc@example.com')
            //->bcc('bcc@example.com')
            //->replyTo('fabien@example.com')
            //->priority(Email::PRIORITY_HIGH)
            ->subject('Time for Symfony Mailer!')
            ->text('Sending emails is fun again!')
            ->html('<p>See Twig integration for better HTML integration!</p>');

        $this->mailer->send($email);

        // ...
    }
}
